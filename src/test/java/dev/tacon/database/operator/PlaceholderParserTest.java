package dev.tacon.database.operator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import dev.tacon.database.operator.placeholder.SqlPlaceholder;

class PlaceholderParserTest {

	private static final PlaceholderResolver RESOLVER = (placeholder, params) -> {
		final SqlPlaceholder sqlPlaceholder = SqlPlaceholder.valueOf(placeholder);
		return switch (sqlPlaceholder) {
			case AUTOINCREMENT -> "AUTO_INCREMENT";
			case BIT -> "BIT";
			case BOOLEAN -> "BOOLEAN";
			case BYTES -> "BYTEA";
			case DATETIME -> "TIMESTAMP";
			case TEXT -> "TEXT";
			case UUID -> "UUID";
			case FN_NOW -> "CURRENT_TIMESTAMP";
			case FN_DATEADDDAYS -> {
				if (params.length == 1) {
					yield "DATE_ADD('DAY', INTERVAL %s DAY)".formatted(params[0]);
				}
				throw new IllegalArgumentException("FN_DATEADDDAYS requires one parameter for the number of days");

			}
			case TRUE -> "TRUE";
			case FALSE -> "FALSE";
			default -> null;
		};
	};

	@Test
	void testColumnDefinition() {
		final String result = PlaceholderParser.parse("column1 ${TEXT}, column2 ${UUID}, column3 ${BOOLEAN}", RESOLVER);
		assertEquals("column1 TEXT, column2 UUID, column3 BOOLEAN", result);
	}

	@Test
	void testDefaultValue() {
		final String result = PlaceholderParser.parse("column1 ${BOOLEAN} DEFAULT ${TRUE}", RESOLVER);
		assertEquals("column1 BOOLEAN DEFAULT TRUE", result);
	}

	@Test
	void testCurrentTimestamp() {
		final String result = PlaceholderParser.parse("column1 ${DATETIME} DEFAULT ${FN_NOW}", RESOLVER);
		assertEquals("column1 TIMESTAMP DEFAULT CURRENT_TIMESTAMP", result);
	}

	@Test
	void testDateAddWithDays() {
		final String result = PlaceholderParser.parse("update tbl set date = ${FN_DATEADDDAYS(3)}", RESOLVER);
		assertEquals("update tbl set date = DATE_ADD('DAY', INTERVAL 3 DAY)", result);
	}

	@Test
	void testDateAddWithoutDays() {
		assertThrows(IllegalArgumentException.class, () -> {
			PlaceholderParser.parse("update tbl set date = ${FN_DATEADDDAYS}", RESOLVER);
		});
	}

	@Test
	void testInvalidPlaceholder() {
		assertThrows(IllegalArgumentException.class, () -> {
			PlaceholderParser.parse("update tbl set date = ${INVALID}", RESOLVER);
		});
	}
}