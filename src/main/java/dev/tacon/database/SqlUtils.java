package dev.tacon.database;

import static java.util.Objects.requireNonNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterators;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import dev.tacon.annotations.NonNull;
import dev.tacon.database.function.ResultSetConsumer;
import dev.tacon.database.function.ResultSetMapper;
import dev.tacon.database.function.TransactionalOperation;

/**
 * Utility class for SQL operations. Provides convenient methods for common SQL tasks such as connection, querying, updating, and more.
 * This class is not intended for instantiation.
 */
public final class SqlUtils {

	private SqlUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/**
	 * Executes a SQL query.
	 *
	 * @param connection a Connection object.
	 * @param query SQL query string.
	 * @return {@code true} if the first result is a ResultSet object,
	 *         {@code false} if it is an update count or there are no results.
	 * @throws SQLException if a database error occurs.
	 */
	public static boolean execute(final @NonNull Connection connection, final @NonNull String query) throws SQLException {
		try (final Statement st = connection.createStatement()) {
			return st.execute(query);
		}
	}

	/**
	 * Executes an SQL Data Manipulation Language (DML) statement, such as INSERT, UPDATE or DELETE;
	 * or an SQL statement that returns nothing, such as a DDL statement.
	 *
	 * @param connection the database connection to be used for the operation.
	 *            Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @return the number of rows affected by the query.
	 * @throws SQLException if a database error occurs during the execution of the query.
	 */
	public static int executeUpdate(final @NonNull Connection connection, final @NonNull String query) throws SQLException {
		try (final Statement st = connection.createStatement()) {
			return st.executeUpdate(query);
		}
	}

	/**
	 * Executes an SQL Data Manipulation Language (DML) statement, such as INSERT, UPDATE or DELETE;
	 * or an SQL statement that returns nothing, such as a DDL statement, using a PreparedStatement.
	 *
	 * @param connection the database connection to be used for the operation.
	 *            Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @param params the parameters to be set in the PreparedStatement. Can be varargs and zero-length.
	 * @return the number of rows affected by the query.
	 * @throws SQLException if a database error occurs during the preparation or execution of the query.
	 */
	public static int executeUpdate(final @NonNull Connection connection, final @NonNull String query, final Object... params) throws SQLException {
		try (final PreparedStatement st = statement(connection, query, params)) {
			return st.executeUpdate();
		}
	}

	/**
	 * Executes multiple SQL Data Manipulation Language (DML) statements, such as INSERT, UPDATE, or DELETE;
	 * or SQL statements that return nothing, such as DDL statements, in a single batch using a Statement.
	 *
	 * @param connection the database connection to be used for the operation.
	 *            Must not be null.
	 * @param queries an array of SQL queries to be executed. Must not be null.
	 * @return the total number of rows affected by all the queries.
	 * @throws SQLException if a database error occurs during the preparation or execution of any of the queries.
	 */
	public static int executeUpdates(final @NonNull Connection connection, final @NonNull String... queries) throws SQLException {
		int affectedRecords = 0;
		if (queries.length > 0) {
			try (final Statement st = connection.createStatement()) {
				for (final String query : queries) {
					affectedRecords += st.executeUpdate(query);
				}
			}
		}
		return affectedRecords;
	}

	/**
	 * Executes multiple SQL Data Manipulation Language (DML) statements, such as INSERT, UPDATE, or DELETE,
	 * in a single transaction using a Statement.
	 *
	 * @param connection the database connection to be used for the transaction. Must not be null.
	 * @param queries an array of SQL queries to be executed within the transaction. Must not be null.
	 * @return the total number of rows affected by all the queries within the transaction.
	 * @throws SQLException if a database error occurs during the preparation or execution of any of the queries.
	 */
	public static int executeTransactionalUpdates(final @NonNull Connection connection, final @NonNull String... queries) throws SQLException {
		final AtomicInteger affectedRecords = new AtomicInteger(0);
		if (queries.length > 0) {
			transaction(connection, c -> {
				try (final Statement st = connection.createStatement()) {
					for (final String query : queries) {
						affectedRecords.addAndGet(st.executeUpdate(query));
					}
				}
			});
		}
		return affectedRecords.intValue();
	}

	/**
	 * Executes a SQL SELECT query and processes the resulting ResultSet row by row.
	 *
	 * @param connection the database connection to be used for the query. Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @param resultSetRowConsumer a consumer function to process each row of the ResultSet. Must not be null.
	 * @param params Optional parameters for the PreparedStatement.
	 * @throws SQLException if a database error occurs.
	 */
	public static void executeQuery(final @NonNull Connection connection, final @NonNull String query, final @NonNull ResultSetConsumer resultSetRowConsumer, final Object... params) throws SQLException {
		try (final PreparedStatement ps = statementRO(connection, query, params)) {
			try (final ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					resultSetRowConsumer.accept(rs);
				}
			}
		}
	}

	/**
	 * Executes a SQL SELECT query and collects the resulting rows into a List.
	 *
	 * @param <T> the type of objects to be collected in the List.
	 * @param conn the database connection to be used for the query. Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @param rowSupplier a function to transform each row of the ResultSet into an object of type T. Must not be null.
	 * @param params Optional parameters for the PreparedStatement.
	 * @return a List of objects, each corresponding to a row in the ResultSet.
	 * @throws SQLException if a database error occurs.
	 */
	public static <T> List<T> executeAndGet(final @NonNull Connection conn, final @NonNull String query, final @NonNull ResultSetMapper<T> rowSupplier, final Object... params) throws SQLException {
		final List<T> data = new ArrayList<>();
		executeQuery(conn, query, rs -> data.add(rowSupplier.apply(rs)), params);
		return data;
	}

	/**
	 * Executes a SQL SELECT query and returns the resulting rows as a List of strings.
	 *
	 * @param conn the database connection to be used for the query. Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @return a List of strings, each string corresponding to a row in the ResultSet.
	 * @throws SQLException if a database error occurs.
	 */
	public static List<String> executeAndGet(final @NonNull Connection conn, final @NonNull String query) throws SQLException {
		final List<String> data = new ArrayList<>();
		executeAndGet(conn, query, data);
		return data;
	}

	/**
	 * Executes a SQL SELECT query and fills the provided output collection with the resulting rows.
	 *
	 * @param conn the database connection to be used for the query. Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @param output the output collection where the results will be stored. Must not be null.
	 * @throws SQLException if a database error occurs.
	 */
	public static void executeAndGet(final @NonNull Connection conn, final @NonNull String query, final @NonNull Collection<String> output) throws SQLException {
		try (final Statement st = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
			try (final ResultSet rs = st.executeQuery(query)) {
				final int columns = rs.getMetaData().getColumnCount();
				if (columns == 1) {
					while (rs.next()) {
						output.add(rs.getString(1));
					}
				} else {
					// la tabella ha più colonne: concateno i campi usando uno StringBuilder
					while (rs.next()) {
						final StringBuilder sb = new StringBuilder();
						for (int i = 1; i <= columns; i++) {
							sb.append(rs.getString(i)).append(" ");
						}
						output.add(sb.toString().trim());
					}
				}
			}
		}
	}

	/**
	 * Executes a SQL SELECT query and returns the first result as an object of the specified type.
	 *
	 * @param <T> the type of the object to be returned.
	 * @param conn the database connection to be used for the query. Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @param resultClass the class of the result to be returned.
	 * @return the first result as an object of the specified type, or null if no result is found.
	 * @throws SQLException if a database error occurs.
	 */
	public static <T> T executeAndGetSingleResult(final @NonNull Connection conn, final @NonNull String query, final @NonNull Class<T> resultClass) throws SQLException {
		try (final Statement st = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
			try (final ResultSet rs = st.executeQuery(query)) {
				if (rs.next()) {
					return rs.getObject(1, resultClass);
				}
				return null;
			}
		}
	}

	/**
	 * Executes a SQL SELECT query and returns the value of a specified column in the first row.
	 *
	 * @param conn the database connection to be used for the query. Must not be null.
	 * @param query the SQL query to be executed. Must not be null.
	 * @param column the name of the column whose value is to be returned.
	 * @return the value of the specified column in the first row as a string, or null if no result is found.
	 * @throws SQLException if a database error occurs.
	 */
	public static String executeAndGetFirst(final @NonNull Connection conn, final @NonNull String query, final @NonNull String column) throws SQLException {
		try (final Statement st = conn.createStatement(); ResultSet rs = st.executeQuery(query)) {
			if (rs.next()) {
				return Objects.toString(rs.getObject(column), null);
			}
			return null;
		}
	}

	/**
	 * Prepares a SQL statement with the given connection, SQL string, and parameters.
	 *
	 * @param connection the database connection to use.
	 * @param sql the SQL query string.
	 * @param params the parameters to be used in the prepared statement.
	 * @return a PreparedStatement object that contains the precompiled SQL statement.
	 * @throws SQLException if an error occurs while preparing the statement.
	 */
	public static PreparedStatement statement(final Connection connection, final String sql, final Object... params) throws SQLException {
		final PreparedStatement statement = connection.prepareStatement(sql);
		for (int i = 0; i < params.length; i++) {
			statement.setObject(i + 1, params[i]);
		}
		return statement;
	}

	/**
	 * Prepares a read-only SQL statement with the given connection, SQL string, and parameters.
	 *
	 * @see {@linkplain ResultSet#TYPE_FORWARD_ONLY}
	 * @see {@linkplain ResultSet#CONCUR_READ_ONLY}
	 *
	 * @param connection the database connection to use.
	 * @param sql the SQL query string.
	 * @param params the parameters to be used in the prepared statement.
	 * @return a PreparedStatement object that is read-only and contains the precompiled SQL statement.
	 * @throws SQLException if an error occurs while preparing the statement.
	 */
	public static PreparedStatement statementRO(final Connection connection, final String sql, final Object... params) throws SQLException {
		final PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		for (int i = 0; i < params.length; i++) {
			statement.setObject(i + 1, params[i]);
		}
		return statement;
	}

	/**
	 * Checks if a query returns any results.
	 *
	 * @param connection the database connection to use. Must not be null.
	 * @param query the SQL query to execute. Must not be null.
	 * @return {@code true} if the query returns at least one result, {@code false} otherwise.
	 * @throws SQLException if a database error occurs.
	 */
	@SuppressWarnings("resource")
	public static <T> Optional<T> first(final @NonNull PreparedStatement ps, final @NonNull ResultSetMapper<T> mapper) throws SQLException {
		requireNonNull(ps, "Statement is null");
		requireNonNull(mapper, "Mapping function is null");
		ps.setMaxRows(1);
		try (ResultSet rs = ps.executeQuery()) {
			return rs.next()
					? Optional.of(mapper.apply(rs))
					: Optional.empty();
		}
	}

	/**
	 * Creates a stream from a PreparedStatement by applying a mapping function to each result in a ResultSet.
	 *
	 * @param <T> the type of the object to be streamed.
	 * @param ps the PreparedStatement to execute. Must not be null.
	 * @param mapper the function that maps the ResultSet to an object of type T. Must not be null.
	 * @return a Stream of objects of type T.
	 * @throws SQLException if a database error occurs.
	 */
	@SuppressWarnings("resource")
	public static <T> Stream<T> stream(final @NonNull PreparedStatement ps, final @NonNull ResultSetMapper<T> mapper) throws SQLException {
		requireNonNull(ps, "Statement is null");
		requireNonNull(mapper, "Mapping function is null");
		final ResultSetIterator<T> iterator = new ResultSetIterator<>(ps, mapper);
		return StreamSupport
				.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false)
				.onClose(iterator::close);
	}

	/**
	 * Checks if a query returns any results.
	 *
	 * @param connection the database connection to use. Must not be null.
	 * @param query the SQL query to execute. Must not be null.
	 * @return {@code true} if the query returns at least one result, {@code false} otherwise.
	 * @throws SQLException if a database error occurs.
	 */
	public static boolean exists(final @NonNull Connection connection, final @NonNull String query) throws SQLException {
		requireNonNull(query, "Query not specified");
		try (final Statement st = connection.createStatement()) {
			try (final ResultSet rs = st.executeQuery(query)) {
				return rs.next();
			}
		}
	}

	/**
	 * Checks if a query with parameters returns any results.
	 *
	 * @param connection the database connection to use.
	 * @param query the SQL query string to execute.
	 * @param params Query parameters.
	 * @return {@code true} if the query returns at least one result, {@code false} otherwise.
	 * @throws SQLException if a database error occurs.
	 */
	public static boolean exists(final @NonNull Connection connection, final @NonNull String query, final Object... params) throws SQLException {
		requireNonNull(query, "Query not specified");
		try (final PreparedStatement st = statementRO(connection, query, params); final ResultSet rs = st.executeQuery()) {
			return rs.next();
		}
	}

	/**
	 * Counts the number of rows in a given table.
	 *
	 * @param connection the database connection to use.
	 * @param table the name of the table to count rows for.
	 * @return the number of rows in the table.
	 * @throws SQLException if a database error occurs.
	 */
	public static long count(final @NonNull Connection connection, final @NonNull String table) throws SQLException {
		requireNonNull(table, "Table not specified");
		try (final Statement st = connection.createStatement()) {
			try (final ResultSet rs = st.executeQuery("select count(*) from " + table)) {
				rs.next();
				return rs.getLong(1);
			}
		}
	}

	/**
	 * Generates a SQL "IN" clause with the given values, suitable for embedding in a SQL query.
	 *
	 * @param values the values to include in the IN clause.
	 * @return a string representing the IN clause.
	 * @throws IllegalArgumentException if values array is empty or contains a null element.
	 */
	public static @NonNull String in(final @NonNull String... values) {
		if (values.length == 0) {
			throw new IllegalArgumentException("At least one value is required");
		}
		final StringBuilder sb = new StringBuilder();
		sb.append("(");
		for (final String value : values) {
			if (value == null) {
				throw new IllegalArgumentException("value cannot be null");
			}
			sb.append("'").append(value.replace("'", "''")).append("',");
		}
		return sb.substring(0, sb.length() - 1) + ")";
	}

	/**
	 * Executes the given consumer within a database transaction.
	 *
	 * @param connection the database connection to use.
	 * @param consumer a CheckedConsumer that will be run inside a transaction.
	 * @throws SQLException if a database error occurs.
	 */
	public static void transaction(final Connection connection, final TransactionalOperation consumer) throws SQLException {
		final boolean autoCommit = connection.getAutoCommit();
		if (autoCommit) {
			connection.setAutoCommit(false);
		}
		try {
			consumer.runInTransaction(connection);
			if (!autoCommit) {
				connection.commit();
			}
		} catch (final Exception e) {
			connection.rollback();
			throw e instanceof SQLException ? (SQLException) e : new SQLException(e);
		} finally {
			if (autoCommit) {
				connection.setAutoCommit(true);
			}
		}
	}

	/**
	 * Copies data from one table to another, assuming the tables have the same structure.
	 *
	 * @param sourceConnection source database connection.
	 * @param destConnection destination database connection.
	 * @param sourceTableName source table name.
	 * @param destTableName destination table name.
	 * @throws SQLException if a database error occurs, or if the table structures are incompatible.
	 */
	public static void copyTable(final Connection sourceConnection, final Connection destConnection, final String sourceTableName, final String destTableName) throws SQLException {
		try (final Statement st1 = sourceConnection.createStatement(); final ResultSet rs1 = st1.executeQuery("select * from " + sourceTableName)) {

			final Map<String, Integer> c1 = new HashMap<>();
			final Map<String, Integer> c2 = new HashMap<>();

			try (final Statement st2 = destConnection.createStatement(); final ResultSet rs2 = st2.executeQuery("select * from " + destTableName)) {
				final ResultSetMetaData metaData1 = rs1.getMetaData();
				final ResultSetMetaData metaData2 = rs2.getMetaData();

				if (metaData1.getColumnCount() != metaData2.getColumnCount()) {
					throw new SQLException("Column count not matching");
				}

				final int columnCount = metaData1.getColumnCount();

				for (int i = 1; i <= columnCount; i++) {
					c1.put(metaData1.getColumnName(i), Integer.valueOf(i));
					c2.put(metaData2.getColumnName(i), Integer.valueOf(i));
				}

				for (final Map.Entry<String, Integer> entry : c1.entrySet()) {
					final String key = entry.getKey();
					final Integer c2Index = c2.get(key);
					if (c2Index == null) {
						throw new SQLException("Column \"" + key + "\" not found in both source and destination tables");
					}
					if (metaData1.getColumnType(entry.getValue().intValue()) != metaData2.getColumnType(c2Index.intValue())) {
						throw new SQLException("Column \"" + key + "\" has different SQL type between tables");
					}
				}
			}

			final List<String> columnNames = new ArrayList<>(c2.keySet());
			final String insertQuery = "insert into " + destTableName + columnNames.stream().collect(Collectors.joining(",", "(", ")")) + " values " + columnNames.stream().map(c -> "?").collect(Collectors.joining(",", "(", ")"));
			try (PreparedStatement ps = destConnection.prepareStatement(insertQuery)) {
				while (rs1.next()) {
					int ix = 1;
					for (final String column : columnNames) {
						ps.setObject(ix++, rs1.getObject(column));
					}
					ps.executeUpdate();
				}
			}
		}
	}

	/**
	 * Inserts or updates a row in a ResultSet based on whether the ResultSet has a next row.
	 *
	 * @param rs the ResultSet to be modified.
	 * @param insert a CheckedConsumer to run if the ResultSet is empty (for insert).
	 * @param insertUpdate a CheckedConsumer to run whether inserting or updating.
	 * @param update a CheckedConsumer to run if the ResultSet has a next row (for update).
	 * @return {@code true} if a new row was inserted, false if an existing row was updated.
	 * @throws SQLException if a database error occurs.
	 */
	public static boolean insertOrUpdate(final ResultSet rs, final ResultSetConsumer insert, final ResultSetConsumer insertUpdate, final ResultSetConsumer update) throws SQLException {
		boolean isInsert = false;
		if (!rs.next()) {
			isInsert = true;
			rs.moveToInsertRow();
			insert.accept(rs);
		} else {
			update.accept(rs);
		}
		insertUpdate.accept(rs);
		if (isInsert) {
			rs.insertRow();
		} else {
			rs.updateRow();
		}
		return isInsert;
	}

	/**
	 * Iterator that traverses a ResultSet and maps each row to an object of type T.
	 * Implements AutoCloseable so the underlying ResultSet can be closed when iteration is done.
	 */
	private static final class ResultSetIterator<T> implements Iterator<T> {

		private final ResultSet rs;
		private final ResultSetMapper<T> mapper;

		ResultSetIterator(final PreparedStatement ps, final ResultSetMapper<T> mapper) throws SQLException {
			this.rs = ps.executeQuery();
			this.mapper = mapper;
		}

		/**
		 * Checks if the ResultSet has more rows to iterate over.
		 *
		 * @return {@code true} if there are more rows, {@code false} otherwise.
		 */
		@Override
		public boolean hasNext() {
			try {
				final boolean hasNext = this.rs.next();
				if (!hasNext) {
					this.close();
				}
				return hasNext;
			} catch (final SQLException ex) {
				this.close();
				sneakyThrow(ex);
				return false;
			}
		}

		/**
		 * Retrieves the next row from the ResultSet and maps it to an object of type T.
		 *
		 * @return the next row mapped to type T.
		 */
		@Override
		public T next() {
			try {
				return this.mapper.apply(this.rs);
			} catch (final SQLException ex) {
				this.close();
				sneakyThrow(ex);
			}
			return null;
		}

		void close() {
			try {
				if (!this.rs.isClosed()) {
					this.rs.close();
				}
			} catch (@SuppressWarnings("unused") final SQLException ex) {}
		}
	}

	@SuppressWarnings("unchecked")
	private static <R, T extends Throwable> R sneakyThrow(final @NonNull Throwable t) throws T {
		throw (T) requireNonNull(t);
	}
}
