package dev.tacon.database.operator;

import static java.util.Objects.requireNonNull;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.database.SqlUtils;
import dev.tacon.database.function.ResultSetConsumer;
import dev.tacon.database.function.ResultSetMapper;
import dev.tacon.database.operator.placeholder.SqlPlaceholder;

/**
 * Abstract class that provides utility methods
 * to perform database operations.
 * <p>
 * Extensions of this class can be registered
 * as services by exploiting the mechanism of {@link ServiceLoader}
 * and then be called based on the database connection:
 *
 * <pre>
 * DatabaseOperator.of(connection).dropColumn("mytable", "mycolumn");
 * </pre>
 *
 * therefore classes that extend {@code DatabaseOperator} will have to
 * have an empty public constructor.
 * <br>
 * It is also possible to specify a prority that will be used
 * when multiple {@code DatabaseOperator} classes can handle
 * a certain connection.
 * <br>
 * Classes that are already built-in return a priority of 0.
 * Return a priority > 0 to give priority to your own classes
 * over the built-in ones.
 *
 * <p>
 * This class can replace placeholders in the format: ${MYPLACEHOLDER}.
 * <br>
 * Some placeholders are already handled by this class and are present
 * in the enum {@link SqlPlaceholder}.
 * <br>
 * Additional placeholders can be handled by setting a function
 * via method {@linkplain #setPlaceholderResolver(BiFunction)}.
 * In this case, the specified function will take precedence
 * over the resolution of placeholders already handled in the case
 * in which a value {@code != null} is returned.
 */
@SuppressWarnings("resource")
public abstract class DatabaseOperator {

	protected static final Logger LOGGER = System.getLogger(DatabaseOperator.class.getName());

	/**
	 * Factory method to create an appropriate DatabaseOperator for the given Connection.
	 *
	 * @param connection The database connection.
	 * @return The suitable DatabaseOperator instance.
	 * @throws IllegalArgumentException if no suitable DatabaseOperator is found.
	 */
	public static DatabaseOperator of(final Connection connection) {

		requireNonNull(connection, "connection cannot be null");

		DatabaseOperator candidate = null;
		int currentPriority = -1;
		for (final DatabaseOperator databaseOperator : ServiceLoader.load(DatabaseOperator.class)) {
			final int priority = databaseOperator.getPriority(connection);
			if (priority > currentPriority) {
				candidate = databaseOperator;
				currentPriority = priority;
			}
		}
		if (candidate == null) {
			throw new IllegalArgumentException("No DatabaseOperator found for the connection");
		}
		candidate.initialize(connection);
		return candidate;
	}

	protected Connection connection;
	private boolean dryRun;
	private boolean resolvePlaceholders = true;
	private PlaceholderResolver placeholderResolver = (s1, s2) -> null;

	/**
	 * Constructs a DatabaseOperator with the given Connection.
	 *
	 * @param connection The database connection.
	 */
	public DatabaseOperator(final Connection connection) {
		this.initialize(requireNonNull(connection, "connection cannot be null"));
	}

	protected DatabaseOperator() {}

	/**
	 * Sets the underlying connection to be used by this operator, cannot be
	 * called twice.
	 *
	 * @param conn the connection to be used.
	 * @throws IllegalStateException if a connection has been already set.
	 */
	protected final void initialize(final Connection conn) {
		if (this.connection != null) {
			throw new IllegalStateException("Connession already set");
		}
		this.connection = conn;
	}

	/**
	 * Gets the priority of this DatabaseOperator for the given database connection.
	 * <br>
	 * Built-in operators have a priority of 0 (zero).
	 *
	 * @param conn the database connection.
	 * @return an integer representing the priority.
	 */
	protected abstract int getPriority(Connection conn);

	/**
	 * Checks if the DatabaseOperator is in "dry run" mode.
	 * <p>
	 * In "dry run" mode, the DatabaseOperator may simulate operations without actually
	 * executing them on the database. This is useful for testing or debugging.
	 * </p>
	 *
	 * @return {@code true} if the DatabaseOperator is in "dry run" mode, {@code false} otherwise.
	 */
	public boolean isDryRun() {
		return this.dryRun;
	}

	/**
	 * Sets the "dry run" mode for the DatabaseOperator.
	 * <p>
	 * In "dry run" mode, the DatabaseOperator may simulate operations without actually
	 * executing them on the database. This is useful for testing or debugging.
	 * </p>
	 *
	 * @param dryRun {@code true} to enable "dry run" mode, {@code false} to disable it.
	 */
	public void setDryRun(final boolean dryRun) {
		this.dryRun = dryRun;
	}

	/**
	 * Indicates whether placeholders in SQL queries should be resolved.
	 *
	 * @return {@code true} if the placeholders should be resolved, {@code false} otherwise.
	 */
	public boolean isResolvePlaceholders() {
		return this.resolvePlaceholders;
	}

	/**
	 * Sets whether placeholders in SQL queries should be resolved.
	 *
	 * @param resolvePlaceholders if the placeholders should be resolved.
	 */
	public void setResolvePlaceholders(final boolean resolvePlaceholders) {
		this.resolvePlaceholders = resolvePlaceholders;
	}

	/**
	 * Sets the function to resolve placeholders in SQL queries.
	 *
	 * @param placeholderResolver the resolver function, not null.
	 */
	public void setPlaceholderResolver(final @NonNull PlaceholderResolver placeholderResolver) {
		this.placeholderResolver = requireNonNull(placeholderResolver, "Resolver cannot be null");
	}

	/**
	 * Returns the underlying connection.
	 */
	public Connection getConnection() {
		return this.connection;
	}

	/**
	 * Retrieves a list of table names from the given schema.
	 *
	 * @param schema the name of the schema from which to read the table names, not null.
	 * @return a list containing the names of tables present in the specified schema.
	 * @throws SQLException if a database access error occurs.
	 */
	public List<String> readTables(final @NonNull String schema) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		try (ResultSet rs = this.connection.getMetaData().getTables(this.connection.getCatalog(), schema, null, null)) {
			final List<String> result = new ArrayList<>();
			while (rs.next()) {
				result.add(rs.getString(3));
			}
			return result;
		}
	}

	/**
	 * Retrieves a list of table names from the default schema.
	 *
	 * @return a list containing the names of tables present in the default schema.
	 * @throws SQLException if a database access error occurs.
	 */
	public List<String> readTables() throws SQLException {
		return this.readTables(this.getDefaultSchema());
	}

	/**
	 * Creates a table with the specified name and definition in the default schema.
	 * If a table with the same name already exists, no action will be taken.
	 *
	 * @param table the name of the table to be created.
	 * @param definition the SQL definition of the table.
	 * @return {@return {@code true} if the table is created, {@code false} if the table already exists.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if any of the parameters are null.
	 */
	public boolean createTable(final @NonNull String table, final @NonNull String definition) throws SQLException {
		return this.createTable(this.getDefaultSchema(), table, definition);
	}

	/**
	 * Creates a table with the specified name and definition in the given schema.
	 * If a table with the same name already exists, no action will be taken.
	 *
	 * @param schema the name of the schema where the table will be created.
	 * @param table the name of the table to be created.
	 * @param definitions the SQL definition of the table.
	 * @return {@code true} if the table is created, {@code false} if the table already exists.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if any of the parameters are null.
	 */
	public boolean createTable(final @NonNull String schema, final @NonNull String table, final @NonNull String definitions) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(definitions, "table definitions not specified");
		if (!this.existsTable(schema, table)) {
			this.executeSql("create table " + schema + "." + table + "(" + definitions + ")");
			return true;
		}
		return false;
	}

	/**
	 * Drops a table with the specified name from the default schema, if exists.
	 *
	 * @param table the name of the table to be dropped.
	 * @return {@code true} if the table is dropped, {@code false} if the table does not exists.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if the table name is null.
	 */
	public boolean dropTable(final @NonNull String table) throws SQLException {
		return this.dropTable(this.getDefaultSchema(), table);
	}

	/**
	 * Drops a table with the specified name from the given schema, if exists.
	 *
	 * @param schema the name of the schema where the table exists.
	 * @param table the name of the table to be dropped.
	 * @return {@code true} if the table is dropped, {@code false} if the table does not exists.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if either the schema or the table name is null.
	 */
	public boolean dropTable(final @NonNull String schema, final @NonNull String table) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		if (this.existsTable(schema, table)) {
			this.executeSql("drop table " + schema + "." + table);
			return true;
		}
		return false;
	}

	/**
	 * Checks if a table with the specified name from the default schema exists.
	 *
	 * @param table the name of the table.
	 * @return {@code true} if the table exists, {@code false} if it doesn't.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if the table name is null.
	 */
	public boolean existsTable(final @NonNull String table) throws SQLException {
		return this.readTables().stream().anyMatch(t -> t.equalsIgnoreCase(table));
	}

	/**
	 * Checks if a table with the specified name from the given schema exists.
	 *
	 * @param schema the name of the schema.
	 * @param table the name of the table.
	 * @return {@code true} if the table exists, {@code false} if it doesn't.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if any of the required parameters are null.
	 */
	public boolean existsTable(final @NonNull String schema, final @NonNull String table) throws SQLException {
		return this.readTables(schema).stream().anyMatch(t -> t.equalsIgnoreCase(table));
	}

	/**
	 * Checks if a column with the specified name from the given table in the default schema exists.
	 *
	 * @param table the name of the table.
	 * @param column the name of the column.
	 * @return {@code true} if the column exists, {@code false} if it doesn't.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if any of the required parameters are null.
	 */
	public boolean existsColumn(final @NonNull String table, final @NonNull String column) throws SQLException {
		return this.existsColumn(this.getDefaultSchema(), table, column);
	}

	/**
	 * Checks if a column with the specified name from the given schema and table exists.
	 *
	 * @param schema the name of the schema.
	 * @param table the name of the table.
	 * @param column the name of the column.
	 * @return {@code true} if the column exists, {@code false} if it doesn't.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if any of the required parameters are null.
	 */
	public abstract boolean existsColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException;

	/**
	 * Creates a new column in the table specified by the given name and definition.
	 * The column is created after the column specified in 'previousColumn', if given.
	 * If a column with the same name already exists, no action is taken.
	 *
	 * @param table the name of the table to add the new column to.
	 * @param column the name of the new column.
	 * @param definition the SQL definition of the new column.
	 * @param previousColumn the column after which the new column will be added (optional).
	 * @return {@code true} if the column was successfully created, {@code false} otherwise.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if any of the required parameters are null.
	 */
	public boolean createColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn) throws SQLException {
		return this.createColumn(this.getDefaultSchema(), table, column, definition, previousColumn);
	}

	/**
	 * Creates a new column in the schema table specified by the given name and definition.
	 * The column is created after the column specified in 'previousColumn', if given.
	 * If a column with the same name already exists, no action is taken.
	 *
	 * @param schema the name of the schema containing the table.
	 * @param table the name of the table to add the new column to.
	 * @param column the name of the new column.
	 * @param definition the SQL definition of the new column.
	 * @param previousColumn the column after which the new column will be added (optional),
	 *            if empty the new column with be added as first.
	 * @return {@code true} if the column was successfully created, {@code false} otherwise.
	 * @throws SQLException if a database access error occurs.
	 * @throws NullPointerException if any of the required parameters are null.
	 */
	public boolean createColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(column, "column name not specified");
		requireNonNull(definition, "column definition not specified");

		if (this.existsColumn(schema, table, column)) {
			return false;
		}

		final String sql = this.buildCreateColumnStatement(schema, table, column, definition, previousColumn);
		this.executeSql(sql);
		return true;
	}

	/**
	 * Creates a new NOT NULL column in the table specified by the given name and definition.
	 * The column is created after the column specified in 'previousColumn', if provided.
	 * If a column with the same name already exists, the method will attempt to convert it to NOT NULL.
	 * If the column already exists and is NOT NULL, no action is taken.
	 *
	 * @param table the name of the table to add the new NOT NULL column to.
	 * @param column the name of the new NOT NULL column.
	 * @param definition the SQL definition of the new NOT NULL column.
	 * @param previousColumn the column after which the new column will be added (optional),
	 *            if empty the new column with be added as first.
	 * @param defaultValue the default value to set for existing records, used when the column is being created or altered to NOT NULL.
	 * @return {@code true} if the column was created or altered to NOT NULL, {@code false} if the column was already NOT NULL.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean createNotNullableColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final Object defaultValue) throws SQLException {
		return this.createNotNullableColumn(this.getDefaultSchema(), table, column, definition, previousColumn, defaultValue);
	}

	/**
	 * Creates a new NOT NULL column in the specified schema and table.
	 *
	 * @param schema the name of the schema where the table exists.
	 * @param table the name of the table to add the new NOT NULL column to.
	 * @param column the name of the new NOT NULL column.
	 * @param definition the SQL definition of the new NOT NULL column.
	 * @param previousColumn the column after which the new column will be added (optional),
	 *            if empty the new column with be added as first.
	 * @param defaultValue the default value to set for existing records, used when the column is being created or altered to NOT NULL.
	 * @return {@code true} if the column was created or altered to NOT NULL, {@code false} if the column was already NOT NULL.
	 * @throws SQLException if a database access error occurs.
	 * @see #createNotNullableColumn(String, String, String, String, Object)
	 */
	public boolean createNotNullableColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final Object defaultValue) throws SQLException {
		return this.createNotNullableColumn(schema, table, column, definition, previousColumn,
				defaultValue != null ? "?" : null,
				defaultValue != null ? new Object[] { defaultValue } : new Object[0]);
	}

	/**
	 * Creates a new NOT NULL column in the specified table with additional options for setting default values.
	 * This method is useful when you want to specify a custom SQL expression for setting the default value for existing records.
	 *
	 * @param table the name of the table to add the new NOT NULL column to.
	 * @param column the name of the new NOT NULL column.
	 * @param definition the SQL definition of the new NOT NULL column.
	 * @param previousColumn the column after which the new column will be added (optional),
	 *            if empty the new column with be added as first.
	 * @param nonNullValueAssignment the SQL expression used to set default values for existing records.
	 * @param params parameters for the SQL expression, if needed.
	 * @return {@code true} if the column was created or altered to NOT NULL, {@code false} if the column was already NOT NULL.
	 * @throws SQLException if a database access error occurs.
	 * @see #createNotNullableColumn(String, String, String, String, Object)
	 */
	public boolean createNotNullableColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final String nonNullValueAssignment, final Object[] params) throws SQLException {
		return this.createNotNullableColumn(this.getDefaultSchema(), table, column, definition, previousColumn, nonNullValueAssignment, params);
	}

	/**
	 * Creates a new NOT NULL column in the specified schema and table with additional options for setting default values.
	 *
	 * @param schema the name of the schema where the table exists.
	 * @param table the name of the table to add the new NOT NULL column to.
	 * @param column the name of the new NOT NULL column.
	 * @param definition the SQL definition of the new NOT NULL column.
	 * @param previousColumn the column after which the new column will be added (optional),
	 *            if empty the new column with be added as first.
	 * @param nonNullValueAssignment the SQL expression used to set default values for existing records.
	 * @param params parameters for the SQL expression, if needed.
	 * @return {@code true} if the column was created or altered to NOT NULL, {@code false} if the column was already NOT NULL.
	 * @throws SQLException if a database access error occurs.
	 * @see #createNotNullableColumn(String, String, String, String, Object)
	 */
	public boolean createNotNullableColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String previousColumn, final String nonNullValueAssignment, final Object[] params) throws SQLException {
		this.createColumn(schema, table, column, definition, previousColumn);
		if (this.isColumnNotNullable(schema, table, column)) {
			return false;
		}
		if (nonNullValueAssignment != null) {
			final String updateQuery = "update " + schema + "." + table + " set " + column + " = " + nonNullValueAssignment + " where " + column + " is null";
			this.executeSql(updateQuery, params);
		}
		final String query = this.buildColumnToNotNullStatement(schema, table, column, definition);
		this.executeSql(query);
		return true;
	}

	/**
	 * Edits an existing column in a table with the given name and definition. Optionally renames the column.
	 * This method uses the default schema.
	 *
	 * @param table the name of the table containing the column to edit.
	 * @param column the name of the existing column to edit.
	 * @param definition the new SQL definition for the column.
	 * @param newName the new name for the column, if renaming is desired (optional).
	 * @return {@code true} if the column was successfully edited, {@code false} if the column does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean editColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String newName) throws SQLException {
		return this.editColumn(this.getDefaultSchema(), table, column, definition, newName);
	}

	/**
	 * Edits an existing column in a table within the specified schema. Optionally renames the column.
	 *
	 * @param schema the name of the schema where the table exists.
	 * @param table the name of the table containing the column to edit.
	 * @param column the name of the existing column to edit.
	 * @param definition the new SQL definition for the column.
	 * @param newName the new name for the column, if renaming is desired (optional).
	 * @return {@code true} if the column was successfully edited, {@code false} if the column does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean editColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final String newName) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(column, "column name not specified");
		requireNonNull(definition, "column definition not specified");

		if (!this.existsColumn(schema, table, column)) {
			return false;
		}

		this.editExistentColumn(schema, table, column, definition, newName);
		return true;
	}

	/**
	 * Edits an existing column in a table, specifying its nullability and optionally renaming it.
	 * This method uses the default schema.
	 *
	 * @param table the name of the table containing the column to edit.
	 * @param column the name of the existing column to edit.
	 * @param definition the new SQL definition for the column.
	 * @param nullable whether the column should be nullable.
	 * @param newName the new name for the column, if renaming is desired (optional).
	 * @return {@code true} if the column was successfully edited, {@code false} if the column does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean editColumn(final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, final String newName) throws SQLException {
		return this.editColumn(this.getDefaultSchema(), table, column, definition, nullable, newName);
	}

	/**
	 * Edits an existing column in a table within the specified schema, specifying its nullability and optionally renaming it.
	 *
	 * @param schema the name of the schema where the table exists.
	 * @param table the name of the table containing the column to edit.
	 * @param column the name of the existing column to edit.
	 * @param definition the new SQL definition for the column.
	 * @param nullable whether the column should be nullable.
	 * @param newName the new name for the column, if renaming is desired (optional).
	 * @return {@code true} if the column was successfully edited, {@code false} if the column does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean editColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, final String newName) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(column, "column name not specified");
		requireNonNull(definition, "column definition not specified");

		if (!this.existsColumn(schema, table, column)) {
			return false;
		}

		this.editExistentColumn(schema, table, column, definition, nullable, newName);
		return true;
	}

	/**
	 * Drops an existing column from a table with the given name.
	 * This method uses the default schema.
	 *
	 * @param table the name of the table containing the column to drop.
	 * @param column the name of the existing column to drop.
	 * @return {@code true} if the column was successfully dropped, {@code false} if the column does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropColumn(final @NonNull String table, final @NonNull String column) throws SQLException {
		return this.dropColumn(this.getDefaultSchema(), table, column);
	}

	/**
	 * Drops an existing column from a table within the specified schema.
	 *
	 * @param schema the name of the schema where the table exists.
	 * @param table the name of the table containing the column to drop.
	 * @param column the name of the existing column to drop.
	 * @return {@code true} if the column was successfully dropped, {@code false} if the column does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(column, "column name not specified");

		if (!this.existsColumn(schema, table, column)) {
			return false;
		}
		final String query = this.buildDropColumnStatement(schema, table, column);
		this.executeSql(query);
		return true;
	}

	/**
	 * Checks if an index exists in the given table within the default schema.
	 *
	 * @param table the name of the table to check.
	 * @param index the name of the index to look for.
	 * @return {@code true} if the index exists, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean existsIndex(final @NonNull String table, final @NonNull String index) throws SQLException {
		return this.existsIndex(this.getDefaultSchema(), table, index);
	}

	/**
	 * Checks if an index exists in the given table within the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table to check.
	 * @param index the name of the index to look for.
	 * @return {@code true} if the index exists, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public abstract boolean existsIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException;

	/**
	 * Creates a new index on the given columns in the specified table within the default schema.
	 *
	 * @param table the name of the table where the index will be created.
	 * @param columns the columns to index.
	 * @param index the name of the index.
	 * @param unique whether the index should enforce unique constraints.
	 * @return {@code true} if the index was successfully created, {@code false} if already exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean createIndex(final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) throws SQLException {
		return this.createIndex(this.getDefaultSchema(), table, columns, index, unique);
	}

	/**
	 * Creates a new index on the given columns in the specified table within the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table where the index will be created.
	 * @param columns the columns to index.
	 * @param index the name of the index.
	 * @param unique whether the index should enforce unique constraints.
	 * @return {@code true} if the index was successfully created, {@code false} if already exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean createIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(columns, "column names not specified");
		requireNonNull(index, "index name not specified");

		final String indexFixed = this.fixIndexName(index);
		if (this.existsIndex(schema, table, indexFixed)) {
			return false;
		}

		final String query = this.buildCreateIndexStatement(schema, table, columns, indexFixed, unique);
		this.executeSql(query);
		return true;
	}

	/**
	 * Renames an existing index in the given table within the default schema.
	 *
	 * @param table the name of the table where the index resides.
	 * @param index the current name of the index.
	 * @param name the new name for the index.
	 * @return {@code true} if the index was successfully renamed, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean renameIndex(final @NonNull String table, final @NonNull String index, final @NonNull String name) throws SQLException {
		return this.renameIndex(this.getDefaultSchema(), table, index, name);
	}

	/**
	 * Renames an existing index in the given table within the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table where the index resides.
	 * @param index the current name of the index.
	 * @param name the new name for the index.
	 * @return {@code true} if the index was successfully renamed, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean renameIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(index, "index name not specified");
		requireNonNull(name, "index new name not specified");

		final String indexFixed = this.fixIndexName(index);

		if (!this.existsIndex(schema, table, indexFixed)) {
			return false;
		}
		final String nameFixed = this.fixIndexName(name);
		if (indexFixed.equals(nameFixed)) {
			return true;
		}
		final String query = this.buildRenameIndexStatement(schema, table, indexFixed, nameFixed);
		this.executeSql(query);
		return true;
	}

	/**
	 * Drops an index from the given table within the default schema.
	 *
	 * @param table the name of the table where the index resides.
	 * @param index the name of the index to drop.
	 * @return {@code true} if the index was successfully dropped, {@code false} if does not exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropIndex(final @NonNull String table, final @NonNull String index) throws SQLException {
		return this.dropIndex(this.getDefaultSchema(), table, index);
	}

	/**
	 * Drops an index from the given table within the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table where the index resides.
	 * @param index the name of the index to drop.
	 * @return {@code true} if the index was successfully dropped, {@code false} if does not exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(index, "index name not specified");

		if (!this.existsIndex(schema, table, index)) {
			return false;
		}
		final String query = this.buildDropIndexStatement(schema, table, index);
		this.executeSql(query);
		return true;
	}

	/**
	 * Creates a CHECK constraint on a table in the default schema.
	 *
	 * @param table the name of the table where the constraint will be created.
	 * @param constraint the name of the CHECK constraint.
	 * @param condition the condition for the CHECK constraint.
	 * @return {@code true} if the CHECK constraint is created, {@code false} if already exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean createCheck(final @NonNull String table, final @NonNull String constraint, final @NonNull String condition) throws SQLException {
		return this.createCheck(this.getDefaultSchema(), table, constraint, condition);
	}

	/**
	 * Creates a CHECK constraint on a table in the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table where the constraint will be created.
	 * @param constraint the name of the CHECK constraint.
	 * @param condition the condition for the CHECK constraint.
	 * @return {@code true} if the CHECK constraint is created, {@code false} if already exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean createCheck(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String condition) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(constraint, "constraint name not specified");
		requireNonNull(condition, "check condition not specified");

		if (this.existsConstraint(schema, table, constraint, ConstraintType.CHECK)) {
			return false;
		}
		final String query = "alter table " + schema + "." + table + " add constraint " + constraint + " check (" + condition + ")";
		this.executeSql(query);
		return true;
	}

	/**
	 * Drops a CHECK constraint from a table in the default schema.
	 *
	 * @param table the name of the table where the constraint resides.
	 * @param constraint the name of the CHECK constraint to drop.
	 * @return {@code true} if the CHECK constraint is successfully dropped, {@code false} if does not exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropCheck(final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.dropCheck(this.getDefaultSchema(), table, constraint);
	}

	/**
	 * Drops a CHECK constraint from a table in the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table where the constraint resides.
	 * @param constraint the name of the CHECK constraint to drop.
	 * @return {@code true} if the CHECK constraint is successfully dropped, {@code false} if does not exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropCheck(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(constraint, "constraint name not specified");

		if (!this.existsConstraint(schema, table, constraint, ConstraintType.CHECK)) {
			return false;
		}
		final String query = this.buildDropCheckStatement(schema, table, constraint);
		this.executeSql(query);
		return true;
	}

	/**
	 * Checks if a constraint exists on a table in the default schema.
	 *
	 * @param table the name of the table where the constraint may reside.
	 * @param constraint the name of the constraint to check for.
	 * @param type the type of constraint to look for.
	 * @return {@code true} if the constraint exists, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean existsConstraint(final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		return this.existsConstraint(this.getDefaultSchema(), table, constraint, type);
	}

	/**
	 * Checks if a constraint exists on a table in the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table where the constraint may reside.
	 * @param constraint the name of the constraint to check for.
	 * @param type the type of constraint to look for.
	 * @return {@code true} if the constraint exists, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public abstract boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException;

	/**
	 * Renames the primary key constraint of a table in the default schema.
	 *
	 * @param table the name of the table whose primary key is to be renamed.
	 * @param newName the new name for the primary key constraint.
	 * @return {@code true} if the renaming is successful, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean renamePrimaryKey(final @NonNull String table, final @NonNull String newName) throws SQLException {
		return this.renamePrimaryKey(this.getDefaultSchema(), table, newName);
	}

	/**
	 * Renames the primary key constraint of a table in the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table whose primary key is to be renamed.
	 * @param newName the new name for the primary key constraint.
	 * @return {@code true} if the renaming is successful, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean renamePrimaryKey(final @NonNull String schema, final @NonNull String table, final @NonNull String newName) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(newName, "constraint new name not specified");

		final String constraintFixed = this.getPrimaryKeyName(schema, table);
		if (constraintFixed == null) {
			return false;
		}
		final String newNameFixed = this.fixPrimaryKeyName(newName);
		if (newNameFixed.equals(constraintFixed)) {
			return true;
		}
		final String query = this.buildRenamePrimaryKeyStatement(schema, table, constraintFixed, newNameFixed);
		this.executeSql(query);
		return true;
	}

	/**
	 * Retrieves the name of the primary key constraint of a table in the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table whose primary key name is to be retrieved.
	 * @return the name of the primary key or {@code null} if it doesn't exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public String getPrimaryKeyName(final @NonNull String schema, final @NonNull String table) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		return this.findPrimaryKeyName(schema, table);
	}

	/**
	 * Retrieves the name of the primary key constraint of a table in the default schema.
	 *
	 * @param table the name of the table whose primary key name is to be retrieved.
	 * @return the name of the primary key or {@code null} if it doesn't exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public String getPrimaryKeyName(final @NonNull String table) throws SQLException {
		return this.getPrimaryKeyName(this.getDefaultSchema(), table);
	}

	/**
	 * Fixes the name of the primary key constraint according to specific rules or formats.
	 *
	 * @param primaryKeyName the original name of the primary key constraint.
	 * @return the fixed name.
	 */
	protected String fixPrimaryKeyName(final @NonNull String primaryKeyName) {
		return primaryKeyName;
	}

	/**
	 * Finds the primary key name for a table in the specified schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table.
	 * @return the name of the primary key or {@code null} if it doesn't exist.
	 * @throws SQLException if a database access error occurs.
	 */
	protected String findPrimaryKeyName(final @NonNull String schema, final @NonNull String table) throws SQLException {
		try (ResultSet rs = this.connection.getMetaData().getPrimaryKeys(this.connection.getCatalog(), schema, table)) {
			if (rs.next()) {
				return rs.getString(6);
			}
			return null;
		}
	}

	protected abstract String buildRenamePrimaryKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String oldName, final @NonNull String newName);

	/**
	 * Checks if a foreign key constraint exists on a table in the default schema.
	 *
	 * @param table the name of the table to check.
	 * @param constraint the name of the foreign key constraint to check for.
	 * @return {@code true} if the foreign key exists, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean existsForeignKey(final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.existsForeignKey(this.getDefaultSchema(), table, constraint);
	}

	/**
	 * Checks if a foreign key constraint exists on a table in a specific schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table to check.
	 * @param constraint the name of the foreign key constraint to check for.
	 * @return {@code true} if the foreign key exists, otherwise {@code false}.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean existsForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.existsConstraint(schema, table, constraint, ConstraintType.FOREIGN_KEY);
	}

	/**
	 * Creates a new foreign key constraint on a table in the default schema.
	 *
	 * @param table the name of the table on which to create the foreign key.
	 * @param referencedTable the name of the table to which the foreign key refers.
	 * @param constraint the name of the foreign key constraint.
	 * @param columns the columns involved in the foreign key.
	 * @param referencedColumns the columns in the referenced table.
	 * @return {@code true} if the foreign key is created, {@code false} if already exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean createForeignKey(final @NonNull String table, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) throws SQLException {
		final String defaultSchema = this.getDefaultSchema();
		return this.createForeignKey(defaultSchema, table, defaultSchema, referencedTable, constraint, columns, referencedColumns);
	}

	/**
	 * Creates a new foreign key constraint on a table in a specific schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table on which to create the foreign key.
	 * @param referencedTableSchema the schema of the referenced table.
	 * @param referencedTable the name of the table to which the foreign key refers.
	 * @param constraint the name of the foreign key constraint.
	 * @param columns the columns involved in the foreign key.
	 * @param referencedColumns the columns in the referenced table.
	 * @return {@code true} if the foreign key is created, {@code false} if already exists.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean createForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String referencedTableSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(referencedTableSchema, "referenced table schema name not specified");
		requireNonNull(referencedTable, "referenced table name not specified");
		requireNonNull(constraint, "constraint name not specified");
		requireNonNull(columns, "column names not specified");
		requireNonNull(referencedColumns, "referenced column names not specified");

		final String constraintFixed = this.fixForeignKeyName(constraint);

		if (this.existsForeignKey(schema, table, constraintFixed)) {
			return false;
		}

		final String query = this.buildCreateForeignKeyStatement(schema, table, referencedTableSchema, referencedTable, constraintFixed, columns, referencedColumns);
		this.executeSql(query);
		return true;
	}

	/**
	 * Fixes the name of the index according to specific rules or formats.
	 *
	 * @param index the name of the index.
	 * @return the fixed index name.
	 */
	protected String fixIndexName(final String index) {
		return index;
	}

	/**
	 * Fixes the name of the foreign key according to specific rules or formats.
	 *
	 * @param constraint the name of the foreign key constraint.
	 * @return the fixed foreign key name.
	 */
	protected String fixForeignKeyName(final String constraint) {
		return constraint;
	}

	/**
	 * Renames a foreign key constraint on a table in the default schema.
	 *
	 * @param table the name of the table containing the foreign key to rename.
	 * @param constraint the current name of the foreign key constraint to rename.
	 * @param newName the new name to set for the foreign key constraint.
	 * @return {@code true} if the rename is successful, {@code false} if the contraint does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean renameForeignKey(final @NonNull String table, final @NonNull String constraint, final @NonNull String newName) throws SQLException {
		return this.renameForeignKey(this.getDefaultSchema(), table, constraint, newName);
	}

	/**
	 * Renames a foreign key constraint on a table in a specific schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table containing the foreign key to rename.
	 * @param constraint the current name of the foreign key constraint to rename.
	 * @param newName the new name to set for the foreign key constraint.
	 * @return {@code true} if the rename is successful, {@code false} if the contraint does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean renameForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String newName) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(constraint, "constraint name not specified");
		requireNonNull(newName, "constraint new name not specified");

		final String constraintFixed = this.fixForeignKeyName(constraint);
		if (!this.existsForeignKey(schema, table, constraintFixed)) {
			return false;
		}
		final String newNameFixed = this.fixForeignKeyName(newName);
		if (newNameFixed.equals(constraintFixed)) {
			return true;
		}
		final String query = this.buildRenameForeignKeyStatement(schema, table, constraintFixed, newNameFixed);
		this.executeSql(query);
		return true;
	}

	/**
	 * Drops a foreign key constraint from a table in the default schema.
	 *
	 * @param table the name of the table from which to drop the foreign key constraint.
	 * @param constraint the name of the foreign key constraint to drop.
	 * @return {@code true} if the foreign key constraint is dropped, {@code false} if the contraint does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropForeignKey(final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return this.dropForeignKey(this.getDefaultSchema(), table, constraint);
	}

	/**
	 * Drops a foreign key constraint from a table in a specific schema.
	 *
	 * @param schema the schema name.
	 * @param table the name of the table from which to drop the foreign key constraint.
	 * @param constraint the name of the foreign key constraint to drop.
	 * @return {@code true} if the foreign key constraint is dropped, {@code false} if the contraint does not exist.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean dropForeignKey(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException {
		requireNonNull(schema, "schema name not specified");
		requireNonNull(table, "table name not specified");
		requireNonNull(constraint, "constraint name not specified");

		final String constraintFixed = this.fixForeignKeyName(constraint);
		if (!this.existsForeignKey(schema, table, constraintFixed)) {
			return false;
		}

		final String query = this.buildDropForeignKeyStatement(schema, table, constraintFixed);
		this.executeSql(query);
		return true;
	}

	/**
	 * Executes a SQL query after parsing it for placeholders.
	 *
	 * @param query the SQL query with placeholders to be parsed.
	 * @return {@code true} if the first result is a ResultSet object,
	 *         {@code false} if it is an update count or there are no results.
	 * @throws SQLException if a database access error occurs.
	 */
	public boolean execute(final @NonNull String query) throws SQLException {
		final String parseSql = this.parseSql(query);
		if (this.dryRun) {
			return false;
		}
		return SqlUtils.execute(this.connection, parseSql);
	}

	/**
	 * Executes an SQL update statement after parsing it for placeholders.
	 *
	 * @param query the SQL query with placeholders to be parsed.
	 * @return the number of records affected, or -1 if the operation is skipped.
	 * @throws SQLException if a database access error occurs.
	 */
	public int executeUpdate(final @NonNull String query) throws SQLException {
		final String parseSql = this.parseSql(query);
		if (this.dryRun) {
			return -1;
		}
		return SqlUtils.executeUpdate(this.connection, parseSql);
	}

	/**
	 * Executes an SQL update statement with parameters, after parsing it for placeholders.
	 *
	 * @param query the SQL query with placeholders to be parsed.
	 * @param params the parameters to be included in the parsed SQL query.
	 * @return the number of records affected, or -1 if the operation is skipped.
	 * @throws SQLException if a database access error occurs.
	 */
	public int executeUpdate(final @NonNull String query, final Object... params) throws SQLException {
		final String parseSql = this.parseSql(query);
		if (this.dryRun) {
			return -1;
		}
		return SqlUtils.executeUpdate(this.connection, parseSql, params);
	}

	/**
	 * Executes multiple SQL update statements after parsing them for placeholders.
	 *
	 * @param queries the SQL queries with placeholders to be parsed.
	 * @return the total number of records affected, or -1 if the operation is skipped.
	 * @throws SQLException if a database access error occurs.
	 */
	public int executeUpdates(final @NonNull String... queries) throws SQLException {
		if (this.dryRun) {
			for (final String query : queries) {
				this.parseSql(query); // parsed query log
			}
			return -1;
		}
		int affectedRecords = 0;
		if (queries.length > 0) {
			try (final Statement st = this.connection.createStatement()) {
				for (final String query : queries) {
					final String parseSql = this.parseSql(query);
					affectedRecords += st.executeUpdate(parseSql);
				}
			}
		}
		return affectedRecords;
	}

	/**
	 * Executes multiple SQL update statements within a transaction, after parsing them for placeholders.
	 *
	 * @param queries the SQL queries with placeholders to be parsed.
	 * @return the total number of records affected, or -1 if the operation is skipped.
	 * @throws SQLException if a database access error occurs.
	 */
	public int executeTransactionalUpdates(final @NonNull String... queries) throws SQLException {
		if (this.dryRun) {
			for (final String query : queries) {
				this.parseSql(query); // parsed query log
			}
			return -1;
		}
		final AtomicInteger affectedRecords = new AtomicInteger(0);
		if (queries.length > 0) {
			SqlUtils.transaction(this.connection, c -> {
				try (final Statement st = this.connection.createStatement()) {
					for (final String query : queries) {
						final String parseSql = this.parseSql(query);
						affectedRecords.addAndGet(st.executeUpdate(parseSql));
					}
				}
			});
		}
		return affectedRecords.intValue();
	}

	/**
	 * Executes a SQL query, processes the result set, and considers placeholders in the query.
	 *
	 * @param query the SQL query with placeholders to be parsed.
	 * @param resultSetRowConsumer the consumer function for processing each row in the result set.
	 * @param params the parameters to be included in the parsed SQL query.
	 * @throws SQLException if a database access error occurs.
	 */
	public void executeQuery(final @NonNull String query, final @NonNull ResultSetConsumer resultSetRowConsumer, final Object... params) throws SQLException {
		SqlUtils.executeQuery(this.connection, this.parseSql(query), resultSetRowConsumer, params);
	}

	/**
	 * Executes a SQL query, maps the result set to a list of objects, and considers placeholders in the query.
	 *
	 * @param query the SQL query with placeholders to be parsed.
	 * @param rowSupplier the function for mapping each row in the result set.
	 * @param params the parameters to be included in the parsed SQL query.
	 * @return a list of objects mapped from the result set.
	 * @throws SQLException if a database access error occurs.
	 */
	public <T> List<T> executeAndGet(final @NonNull String query, final @NonNull ResultSetMapper<T> rowSupplier, final Object... params) throws SQLException {
		return SqlUtils.executeAndGet(this.connection, this.parseSql(query), rowSupplier, params);
	}

	/**
	 * Parses the SQL query for placeholders and resolves them.
	 *
	 * @param sql the original SQL query that may contain placeholders.
	 * @return the parsed SQL query with all placeholders resolved.
	 */
	protected String parseSql(final String sql) {
		if (this.resolvePlaceholders) {
			final String parsedSql = PlaceholderParser.parse(sql, this::resolvePlaceholder);
			LOGGER.log(Level.INFO, parsedSql);
			return parsedSql;
		}
		LOGGER.log(Level.INFO, sql);
		return sql;
	}

	/**
	 * Resolves a single placeholder in the SQL query.
	 *
	 * @param placeholder the placeholder to resolve.
	 * @param params an optional array of strings representing the parameters.
	 * @return the resolved string to replace the placeholder.
	 */
	protected String resolvePlaceholder(final String placeholder, final String... params) {
		final String resolved = this.placeholderResolver.resolve(placeholder, params);
		if (resolved != null) {
			return resolved;
		}
		final SqlPlaceholder sqlPlaceholder = SqlPlaceholder.valueOf(placeholder);
		return this.resolvePlaceholder(sqlPlaceholder, params);
	}

	/**
	 * Resolves a single placeholder defined by the enum {@code SqlPlaceholder}.
	 *
	 * @param sqlPlaceholder the placeholder.
	 * @param params an optional array of strings representing the parameters.
	 * @return the resolved string to replace the placeholder.
	 */
	protected abstract String resolvePlaceholder(final SqlPlaceholder sqlPlaceholder, final String[] params);

	/**
	 * Builds a SQL statement for creating a new column in a table.
	 *
	 * @param schema the schema of the table.
	 * @param table the table in which the column will be created.
	 * @param column the name of the column to create.
	 * @param definition the definition of the column.
	 * @param previousColumn the name of the column before which to add the new column; may be {@code null}.
	 * @return the SQL statement for creating a new column.
	 * @throws SQLException if a database access error occurs.
	 */
	protected String buildCreateColumnStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String previousColumn) throws SQLException {
		return "alter table " + schema + "." + table + " add " + column + " " + definition;
	}

	/**
	 * Builds a SQL statement to set a column as not nullable.
	 *
	 * @param schema the schema of the table.
	 * @param table the table that contains the column.
	 * @param column the column to set as not nullable.
	 * @param definition the definition of the column.
	 * @return the SQL statement to set the column as not nullable.
	 * @throws SQLException if a database access error occurs.
	 */
	protected abstract String buildColumnToNotNullStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition) throws SQLException;

	/**
	 * Edits an existing column in a table.
	 *
	 * @param schema the schema of the table.
	 * @param table the table that contains the column.
	 * @param column the column to edit.
	 * @param definition the new definition for the column.
	 * @param newName the new name for the column; may be {@code null}.
	 * @throws SQLException if a SQL error occurs.
	 */
	protected abstract void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String newName) throws SQLException;

	/**
	 * Edits an existing column in a table with an option to set it as nullable or not nullable.
	 *
	 * @param schema the schema of the table.
	 * @param table the table that contains the column.
	 * @param column the column to edit.
	 * @param definition the new definition for the column.
	 * @param nullable whether the column should be nullable or not.
	 * @param newName the new name for the column; may be {@code null}.
	 * @throws SQLException if a SQL error occurs.
	 */
	protected abstract void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, boolean nullable, final @Nullable String newName) throws SQLException;

	/**
	 * Builds a SQL statement to drop a column from a table.
	 *
	 * @param schema the schema of the table.
	 * @param table the table that contains the column.
	 * @param column the column to drop.
	 * @return the SQL statement to drop the column.
	 * @throws SQLException if a database access error occurs.
	 */
	protected String buildDropColumnStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column) {
		return "alter table " + schema + "." + table + " drop column " + column;
	}

	/**
	 * Builds a SQL statement for creating an index on a table.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table where the index will be created.
	 * @param columns the columns that will be part of the index.
	 * @param index the name of the index to be created.
	 * @param unique whether the index should enforce unique constraints.
	 * @return a SQL statement as a string.
	 * @throws SQLException if a database access error occurs.
	 */
	protected abstract String buildCreateIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique);

	/**
	 * Builds a SQL statement for renaming an existing index in a table.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table containing the index.
	 * @param index the current name of the index.
	 * @param name the new name for the index.
	 * @return a SQL statement as a string.
	 */
	protected abstract String buildRenameIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) throws SQLException;

	/**
	 * Builds a SQL statement for dropping an existing index from a table.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table containing the index.
	 * @param index the name of the index to be dropped.
	 * @return a SQL statement as a string.
	 * @throws SQLException if a database access error occurs.
	 */
	protected abstract String buildDropIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException;

	/**
	 * Builds a SQL statement for creating a foreign key constraint on a table.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table where the foreign key will be created.
	 * @param referencedTableSchema the schema where the referenced table resides.
	 * @param referencedTable the table that the foreign key references.
	 * @param constraint the name of the foreign key constraint.
	 * @param columns the columns that will be part of the foreign key.
	 * @param referencedColumns the columns in the referenced table.
	 * @return a SQL statement as a string.
	 * @throws SQLException if a database access error occurs.
	 */
	protected abstract String buildCreateForeignKeyStatement(
			final @NonNull String schema,
			final @NonNull String table,
			final @NonNull String referencedTableSchema,
			final @NonNull String referencedTable,
			final @NonNull String constraint,
			final @NonNull String columns,
			final @NonNull String referencedColumns) throws SQLException;

	/**
	 * Builds a SQL statement for renaming an existing foreign key constraint in a table.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table containing the foreign key.
	 * @param constraint the current name of the foreign key constraint.
	 * @param name the new name for the foreign key constraint.
	 * @return a SQL statement as a string.
	 * @throws SQLException if a database access error occurs.
	 */
	protected abstract String buildRenameForeignKeyStatement(
			final @NonNull String schema,
			final @NonNull String table,
			final @NonNull String constraint,
			final @NonNull String name) throws SQLException;

	/**
	 * Builds a SQL statement for dropping an existing foreign key constraint from a table.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table containing the foreign key.
	 * @param constraint the name of the foreign key constraint to be dropped.
	 * @return a SQL statement as a string.
	 * @throws SQLException if a database access error occurs.
	 */
	protected abstract String buildDropForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException;

	/**
	 * Builds a SQL statement for dropping a check constraint from a table.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table containing the check constraint.
	 * @param constraint the name of the check constraint to be dropped.
	 * @return a SQL statement as a string.
	 * @throws SQLException if a database access error occurs.
	 */
	protected String buildDropCheckStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) throws SQLException {
		return "alter table " + schema + "." + table + " drop constraint " + constraint;
	}

	/**
	 * Determines whether a column in a table is not nullable.
	 *
	 * @param schema the name of the schema where the table resides.
	 * @param table the name of the table.
	 * @param column the name of the column to check.
	 * @return {@code true} if the column is not nullable, {@code false} otherwise.
	 * @throws SQLException if a SQL error occurs.
	 */
	protected abstract boolean isColumnNotNullable(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException;

	/**
	 * Internal method for SQL statement execution after parsing it for placeholders.
	 *
	 * @param query the SQL query with placeholders to be parsed.
	 * @param params the parameters to be included in the parsed SQL query.
	 * @throws SQLException if a SQL error occurs.
	 */
	protected final void executeSql(final String sql, final Object... params) throws SQLException {
		final String parsedSql = this.parseSql(sql);

		if (!this.dryRun) {
			try (PreparedStatement ps = SqlUtils.statement(this.connection, parsedSql, params)) {
				ps.execute();
			}
		}
	}

	/**
	 * Type of constraints that can be applied to a database table.
	 */
	public enum ConstraintType {

		UNIQUE("UNIQUE"),
		PRIMARY_KEY("PRIMARY KEY"),
		FOREIGN_KEY("FOREIGN KEY"),
		CHECK("CHECK");

		private final String value;

		ConstraintType(final String value) {
			this.value = value;
		}

		/**
		 * Gets the SQL representation of the constraint.
		 */
		public String getValue() {
			return this.value;
		}
	}

	/**
	 * Retrieves the default schema name associated with the current database connection.
	 * <p>
	 * By default this method delegates the call to the underlying JDBC {@linkplain Connection#getSchema()} method.
	 * </p>
	 *
	 * @return the name of the default schema as a {@code String}. Can be {@code null} if the database does not support schemas.
	 * @throws SQLException if a database access error occurs while retrieving the schema name.
	 */
	protected String getDefaultSchema() throws SQLException {
		return this.connection.getSchema();
	}

	/**
	 * Retrieves the catalog name associated with the current database connection.
	 * <p>
	 * By default this method delegates the call to the underlying JDBC {@linkplain Connection#getCatalog()} method.
	 * </p>
	 *
	 * @return the name of the catalog as a {@code String}. Can be {@code null} if the database does not support catalogs.
	 * @throws SQLException if a database access error occurs while retrieving the catalog name.
	 */
	protected String getCatalog() throws SQLException {
		return this.connection.getCatalog();
	}
}
