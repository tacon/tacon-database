package dev.tacon.database.operator.placeholder;

/**
 * Abstract class that provides a common foundation for different SQL placeholders.
 * This class encapsulates an SQL placeholder and associated parameters, offering utility methods for their manipulation.
 */
public abstract class AbstractPlaceholder {

	private final SqlPlaceholder sqlPlaceholder;
	private final String[] params;

	/**
	 * Constructs an AbstractPlaceholder object.
	 *
	 * @param sqlPlaceholder The SQL placeholder.
	 * @param params The parameters associated with the SQL placeholder.
	 */
	protected AbstractPlaceholder(final SqlPlaceholder sqlPlaceholder, final String... params) {
		this.sqlPlaceholder = sqlPlaceholder;
		this.params = params;
	}

	/**
	 * Retrieves the parameter at the specified index.
	 *
	 * @param i The index of the parameter.
	 * @return The parameter at the specified index.
	 */
	protected String getParam(final int i) {
		return this.params[i];
	}

	/**
	 * Retrieves the complete placeholder expression consisting of the base part and the parameter part.
	 *
	 * @return The complete placeholder expression.
	 */
	public String get() {
		return this.getBasePart() + this.getParamPart();
	}

	/**
	 * Retrieves the base part of the placeholder, which is the name of the SQL placeholder.
	 *
	 * @return The base part of the placeholder.
	 */
	public String getBasePart() {
		return this.sqlPlaceholder.name();
	}

	/**
	 * Retrieves the parameter part of the placeholder, which consists of the parameters enclosed in parentheses.
	 * If no parameters are present, an empty string is returned.
	 *
	 * @return The parameter part of the placeholder.
	 */
	public String getParamPart() {
		if (this.params.length == 0) {
			return "";
		}
		return "(" + String.join(",", this.params) + ")";
	}

	@Override
	public String toString() {
		return "${" + this.get() + "}";
	}
}