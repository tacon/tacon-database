package dev.tacon.database.operator.placeholder;

/**
 * The {@code DateAddDays} class is a concrete implementation of {@link AbstractPlaceholder} that represents
 * the SQL DATEADDDAYS function. This class is intended to be used when you want to perform date addition operations
 * in SQL queries.
 */
public class DateAddDays extends AbstractPlaceholder {

	/**
	 * Constructs a {@code DateAddDays} object with a given date and the number of days to add.
	 *
	 * @param date The date to which days will be added.
	 * @param days The number of days to add to the date.
	 */
	public DateAddDays(final String date, final String days) {
		this(new String[] { date, days });
	}

	/**
	 * Constructs a {@code DateAddDays} object with an array of parameters.
	 * The array should contain exactly two elements: the date and the number of days to add.
	 *
	 * @param params An array of parameters containing the date and the number of days to add.
	 * @throws IllegalArgumentException If the number of parameters is not exactly 2.
	 */
	public DateAddDays(final String... params) {
		super(SqlPlaceholder.FN_DATEADDDAYS, params);
		if (params.length != 2) {
			throw new IllegalArgumentException("Incorrect number of parameters");
		}
	}

	/**
	 * Retrieves the date to which days will be added.
	 *
	 * @return The date as a string.
	 */
	public String getDate() {
		return this.getParam(0);
	}

	/**
	 * Retrieves the number of days to add to the date.
	 *
	 * @return The number of days as a string.
	 */
	public String getDays() {
		return this.getParam(1);
	}
}
