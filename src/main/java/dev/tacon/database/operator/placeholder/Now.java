package dev.tacon.database.operator.placeholder;

/**
 * The {@code Now} class is a concrete implementation of {@link AbstractPlaceholder} that represents the SQL NOW function.
 * This class is intended to be used when you want to insert the current date and time into a SQL query.
 */
public class Now extends AbstractPlaceholder {

	/**
	 * Constructs a {@code Now} object with the SQL placeholder for the NOW function.
	 */
	public Now() {
		super(SqlPlaceholder.FN_NOW);
	}
}
