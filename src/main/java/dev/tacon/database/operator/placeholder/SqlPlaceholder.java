package dev.tacon.database.operator.placeholder;

/**
 * Enumeration that represents various placeholders used in SQL queries.
 * These placeholders can represent data types, functions, or values.
 */
public enum SqlPlaceholder {

	// data types
	AUTOINCREMENT,
	BIT,
	BOOLEAN,
	BYTES,
	DATETIME,
	TEXT,
	UUID,

	// functions
	FN_NOW,
	FN_DATEADDDAYS,

	// values
	TRUE,
	FALSE;
}
