package dev.tacon.database.operator;

import static java.util.Objects.requireNonNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.database.operator.placeholder.DateAddDays;
import dev.tacon.database.operator.placeholder.SqlPlaceholder;

public class MsSqlServerOperator extends InformationSchemaDatabaseOperator {

	public MsSqlServerOperator() {}

	public MsSqlServerOperator(final Connection connection) {
		super(connection);
	}

	@Override
	protected int getPriority(final Connection conn) {
		final String className = conn.getClass().getName();
		return className.startsWith("net.sourceforge.jtds") || className.startsWith("com.microsoft.sqlserver.jdbc") ? 0 : -1;
	}

	@Override
	protected String buildColumnToNotNullStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition) {
		return "alter table " + schema + "." + table + " alter column " + column + " " + definition + " not null";
	}

	@Override
	public boolean existsIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		requireNonNull(schema, "schema not specified");
		requireNonNull(table, "table not specified");
		requireNonNull(index, "index not specified");
		try (PreparedStatement ps = this.connection.prepareStatement("""
				SELECT ind.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id where\s\
				lower(schema_name(t.schema_id)) = ?\s\
				and lower(t.name) = ?\s\
				and lower(ind.name) = ?\s""")) {
			ps.setString(1, schema.toLowerCase());
			ps.setString(2, table.toLowerCase());
			ps.setString(3, index.toLowerCase());
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}
		}
	}

	@Override
	public boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		if (ConstraintType.UNIQUE == type) {
			requireNonNull(table, "table not specified");
			requireNonNull(constraint, "constraint not specified");
			if (!this.existsTable(table)) {
				return false;
			}
			final String query = """
					SELECT ind.is_unique FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id where ind.is_unique = 1 and\s\
					lower(t.name) = ?\s\
					and lower(ind.name) = ?\s""";
			try (PreparedStatement ps = this.connection.prepareStatement(query)) {
				ps.setString(1, table);
				ps.setString(2, constraint);
				try (ResultSet rs = ps.executeQuery()) {
					return rs.next();
				}
			}
		}
		return super.existsConstraint(schema, table, constraint, type);
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, null, newName);
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, @Nullable final String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, Boolean.valueOf(nullable), newName);
	}

	@Override
	protected String buildCreateIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) {
		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("create");
		if (unique) {
			queryBuilder.append(" unique");
		}
		queryBuilder.append(" index ").append(index).append(" on ").append(schema).append(".").append(table)
				.append("(").append(columns).append(")");

		return queryBuilder.toString();
	}

	@Override
	protected String buildRenameIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) {
		return String.format("sp_rename '%s.%s.%s','%s','INDEX'", schema, table, index, name);
	}

	@Override
	protected String buildDropIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index) {
		return "drop index " + index + " on " + schema + "." + table;
	}

	@Override
	protected String buildRenamePrimaryKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String oldName, final @NonNull String newName) {
		return String.format("sp_rename '%s.%s.%s','%s'", schema, table, oldName, newName);
	}

	@Override
	protected String buildCreateForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String referencedTableSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) {
		final StringBuilder sql = new StringBuilder();
		sql.append("alter table ");
		sql.append(schema).append(".").append(table);
		sql.append(" add constraint ").append(constraint);
		sql.append(" foreign key(");
		sql.append(columns);
		sql.append(") references ");
		sql.append(referencedTableSchema).append(".").append(referencedTable);
		sql.append("(").append(referencedColumns).append(")");
		return sql.toString();
	}

	@Override
	protected String buildRenameForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String name) {
		return String.format("sp_rename '%s.%s','%s','OBJECT'", schema, constraint, name);
	}

	@Override
	protected String buildDropForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		return "alter table " + schema + "." + table + " drop constraint " + constraint;
	}

	@Override
	protected String getDefaultSchema() throws SQLException {
		return "dbo";
	}

	@Override
	protected String resolvePlaceholder(final SqlPlaceholder sqlPlaceholder, final String[] params) {
		return switch (sqlPlaceholder) {
			case AUTOINCREMENT -> "int IDENTITY(1,1)";
			case BIT, BOOLEAN -> "bit";
			case BYTES -> "varbinary(max)";
			case DATETIME -> "datetime";
			case TEXT -> "varchar(max)";
			case UUID -> "uniqueidentifier";
			case TRUE -> "1";
			case FALSE -> "0";
			case FN_NOW -> "getdate()";
			case FN_DATEADDDAYS -> {
				final DateAddDays dateAddDays = new DateAddDays(params);
				yield String.format("dateadd(day, %s, %s)", dateAddDays.getDate(), dateAddDays.getDays());
			}
		};
	}

	private void editExistentColumn(final String schema, final String table, final String column, final String definition, final Boolean nullable, final String newName) throws SQLException {
		// edit column
		final StringBuilder sql = new StringBuilder()
				.append("alter table ")
				.append(schema).append(".").append(table)
				.append(" alter column ")
				.append(column)
				.append(" ").append(definition);
		if (nullable != null) {
			sql.append(nullable.booleanValue() ? " null" : " not null");
		}
		this.executeSql(sql.toString());
		// then rename it
		if (newName != null && !column.equalsIgnoreCase(newName)) {
			this.executeSql("EXEC sp_rename '" + schema + "." + table + "." + column + " ', '" + newName + "', 'COLUMN'");
		}
	}
}
