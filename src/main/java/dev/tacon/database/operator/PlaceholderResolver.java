package dev.tacon.database.operator;

/**
 * Defines an interface for resolving SQL placeholders.
 * <p>
 * This interface provides a way to dynamically replace placeholders in SQL queries with actual values.
 * Implementations should handle the placeholder format and resolution logic.
 * </p>
 */
public interface PlaceholderResolver {

	/**
	 * Resolves a given SQL placeholder containing the provided optional parameters.
	 * <p>
	 * Implementations of this method should take a placeholder string and an optional array of parameters,
	 * and return the resolved value that will replace the placeholder in the SQL query.
	 * </p>
	 *
	 * @param placeholder the placeholder string to be resolved.
	 * @param params an optional array of strings representing the parameters.
	 * @return the resolved string that should replace the placeholder in the SQL query.
	 */
	String resolve(String placeholder, String... params);
}
