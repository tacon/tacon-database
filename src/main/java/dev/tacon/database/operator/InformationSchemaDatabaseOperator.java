package dev.tacon.database.operator;

import static java.util.Objects.requireNonNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dev.tacon.annotations.NonNull;

/**
 * DatabaseOperator mainly based on the information schema.
 */
public abstract class InformationSchemaDatabaseOperator extends DatabaseOperator {

	public InformationSchemaDatabaseOperator() {}

	public InformationSchemaDatabaseOperator(final Connection connection) {
		super(connection);
	}

	@Override
	public boolean existsTable(final @NonNull String schema, final @NonNull String table) throws SQLException {
		requireNonNull(schema, "schema not specified");
		requireNonNull(table, "table not specified");
		try (PreparedStatement ps = this.connection.prepareStatement("select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'BASE TABLE' and lower(TABLE_SCHEMA) = ? and lower(TABLE_NAME) = ? order by TABLE_NAME")) {
			ps.setString(1, schema.toLowerCase());
			ps.setString(2, table.toLowerCase());
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}
		}
	}

	@Override
	public boolean existsColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		requireNonNull(schema, "schema not specified");
		requireNonNull(table, "table not specified");
		requireNonNull(column, "column not specified");
		try (PreparedStatement ps = this.connection.prepareStatement("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where lower(TABLE_SCHEMA) = ? and lower(TABLE_NAME) = ? and lower(COLUMN_NAME) = ? order by TABLE_NAME")) {
			ps.setString(1, schema.toLowerCase());
			ps.setString(2, table.toLowerCase());
			ps.setString(3, column.toLowerCase());
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}
		}
	}

	@Override
	public boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		requireNonNull(schema, "schema not specified");
		requireNonNull(table, "table not specified");
		requireNonNull(constraint, "constraint not specified");
		requireNonNull(type, type + " not specified");
		final String catalog = this.getCatalog();
		try (PreparedStatement ps = this.connection.prepareStatement("""
				select CONSTRAINT_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where\
				 TABLE_CATALOG = ?\
				 and TABLE_SCHEMA = ?\
				 and lower(TABLE_NAME) = ?\
				 and lower(CONSTRAINT_NAME) = ?\
				 and CONSTRAINT_TYPE = ?""")) {
			ps.setString(1, catalog);
			ps.setString(2, schema);
			ps.setString(3, table.toLowerCase());
			ps.setString(4, constraint.toLowerCase());
			ps.setString(5, type.getValue());
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}
		}
	}

	@Override
	protected boolean isColumnNotNullable(final @NonNull String schema, final @NonNull String table, final @NonNull String column) throws SQLException {
		requireNonNull(schema, "schema not specified");
		requireNonNull(table, "table not specified");
		requireNonNull(column, "column not specified");
		try (PreparedStatement ps = this.connection.prepareStatement("select IS_NULLABLE from INFORMATION_SCHEMA.COLUMNS where lower(TABLE_SCHEMA) = ? and lower(TABLE_NAME) = ? and lower(COLUMN_NAME) = ? order by TABLE_NAME")) {
			ps.setString(1, schema.toLowerCase());
			ps.setString(2, table.toLowerCase());
			ps.setString(3, column.toLowerCase());
			try (ResultSet rs = ps.executeQuery()) {
				rs.next();
				return "NO".equalsIgnoreCase(rs.getString("IS_NULLABLE"));
			}
		}
	}
}
