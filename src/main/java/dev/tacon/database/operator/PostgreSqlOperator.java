package dev.tacon.database.operator;

import static java.util.Objects.requireNonNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.database.operator.placeholder.DateAddDays;
import dev.tacon.database.operator.placeholder.SqlPlaceholder;

public class PostgreSqlOperator extends InformationSchemaDatabaseOperator {

	public PostgreSqlOperator(final Connection connection) {
		super(connection);
	}

	public PostgreSqlOperator() {}

	@Override
	protected int getPriority(final Connection conn) {
		return conn.getClass().getName().startsWith("org.postgresql") ? 0 : -1;
	}

	@Override
	public boolean existsIndex(final @NonNull String schema, final @NonNull String table, final @NonNull String index) throws SQLException {
		requireNonNull(schema, "schema not specified");
		requireNonNull(table, "table not specified");
		requireNonNull(index, "index name not specified");

		return existsAttribute(this.connection, index, "i");
	}

	@Override
	public boolean existsConstraint(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final ConstraintType type) throws SQLException {
		requireNonNull(schema, "schema not specified");
		requireNonNull(table, "table not specified");
		requireNonNull(constraint, "constraint not specified");
		final String constraint63 = constraint.length() > 63 ? constraint.substring(0, 63) : constraint;

		if (type == ConstraintType.UNIQUE) {

			if (!this.existsTable(table)) {
				return false;
			}

			// TODO is schema necessary?
			final String query = "select 1 from pg_class t, pg_class i, pg_index ix where t.oid = ix.indrelid and i.oid = ix.indexrelid and t.relkind = 'r' and ix.indisunique and not ix.indisprimary and t.relname ilike ? and i.relname ilike ?";
			try (PreparedStatement ps = this.connection.prepareStatement(query)) {
				ps.setString(1, table);
				ps.setString(2, constraint63);
				try (ResultSet rs = ps.executeQuery()) {
					return rs.next();
				}
			}
		}
		return super.existsConstraint(schema, table, constraint63, type);
	}

	@Override
	protected String fixForeignKeyName(final String constraint) {
		return constraint.length() > 63 ? constraint.substring(0, 63) : constraint;
	}

	@Override
	protected String buildColumnToNotNullStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition) {
		return "alter table " + schema + "." + table + " alter " + column + " set not null";
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final @Nullable String newName) throws SQLException {
		// edit column
		this.executeSql(new StringBuilder()
				.append("alter table ")
				.append(schema).append(".").append(table)
				.append(" alter column ")
				.append(column)
				.append(" TYPE ").append(definition).toString());
		// then rename it
		if (newName != null && !column.equalsIgnoreCase(newName)) {
			this.executeSql("alter table " + schema + "." + table + " rename " + column + " to " + newName);
		}
	}

	@Override
	protected void editExistentColumn(final @NonNull String schema, final @NonNull String table, final @NonNull String column, final @NonNull String definition, final boolean nullable, @Nullable final String newName) throws SQLException {
		this.editExistentColumn(schema, table, column, definition, newName);
		// set null/not null
		final Object operation = nullable ? "drop" : "set";
		this.executeSql(String.format("alter table %s.%s alter column %s %s not null", schema, table, newName == null ? column : newName, operation));
	}

	@Override
	protected String buildCreateIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String columns, final @NonNull String index, final boolean unique) {
		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("create");
		if (unique) {
			queryBuilder.append(" unique");
		}
		queryBuilder.append(" index ").append(index).append(" on ").append(schema).append(".").append(table)
				.append("(").append(columns).append(")");

		return queryBuilder.toString();
	}

	@Override
	protected String buildRenameIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index, final @NonNull String name) {
		return "alter index " + index + " rename to " + name;
	}

	@Override
	protected String buildDropIndexStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String index) {
		return "drop index " + index;
	}

	@Override
	protected String buildRenamePrimaryKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String oldName, final @NonNull String newName) {
		return String.format("ALTER TABLE %s.%s RENAME CONSTRAINT %s TO %s", schema, table, oldName, newName);
	}

	@Override
	protected String fixPrimaryKeyName(final @NonNull String primaryKeyName) {
		return primaryKeyName.length() > 63 ? primaryKeyName.substring(0, 63) : primaryKeyName;
	}

	@Override
	protected String buildCreateForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String referencedTableSchema, final @NonNull String referencedTable, final @NonNull String constraint, final @NonNull String columns, final @NonNull String referencedColumns) {
		final StringBuilder sql = new StringBuilder();
		sql.append("alter table ");
		sql.append(schema).append(".").append(table);
		sql.append(" add constraint ").append(constraint);
		sql.append(" foreign key(");
		sql.append(columns);
		sql.append(") references ");
		sql.append(referencedTableSchema).append(".").append(referencedTable);
		sql.append("(").append(referencedColumns).append(")");
		return sql.toString();
	}

	@Override
	protected String buildRenameForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint, final @NonNull String name) {
		return "alter table " + schema + "." + table + " rename constraint " + constraint + " to " + name;
	}

	@Override
	protected String buildDropForeignKeyStatement(final @NonNull String schema, final @NonNull String table, final @NonNull String constraint) {
		return "alter table " + schema + "." + table + " drop constraint " + constraint;
	}

	@Override
	protected String resolvePlaceholder(final SqlPlaceholder sqlPlaceholder, final String[] params) {
		switch (sqlPlaceholder) {
			case AUTOINCREMENT:
				return "serial";
			case BIT:
			case BOOLEAN:
				return "boolean";
			case BYTES:
				return "bytea";
			case DATETIME:
				return "timestamp";
			case TEXT:
				return "text";
			case UUID:
				return "uuid";
			case TRUE:
				return "true";
			case FALSE:
				return "false";
			case FN_NOW:
				return "now()";
			case FN_DATEADDDAYS:
				final DateAddDays dateAddDays = new DateAddDays(params);
				return String.format("%s + %s * interval '1 day'", dateAddDays.getDate(), dateAddDays.getDays());
			default:
				break;
		}
		throw new UnsupportedOperationException("Unknown placeholder " + sqlPlaceholder);
	}

	private static boolean existsAttribute(final Connection connection, final String name, final String kind) throws SQLException {

		// https://www.postgresql.org/docs/9.3/static/catalog-pg-class.html

		final String query = """
				SELECT 1 FROM pg_catalog.pg_class c\s\
				JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace\s\
				WHERE n.nspname = CURRENT_SCHEMA()\s\
				AND n.nspname NOT LIKE 'pg_%'\s\
				AND c.relname ilike ?\s\
				AND c.relkind = ?"""; // attribute type

		try (PreparedStatement ps = connection.prepareStatement(query)) {
			ps.setString(1, name);
			ps.setString(2, kind);
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}
		}
	}
}
