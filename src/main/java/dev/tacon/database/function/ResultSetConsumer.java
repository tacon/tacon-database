package dev.tacon.database.function;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A functional interface representing an operation that accepts a single {@link ResultSet} argument
 * and returns no result. The operation may throw an {@link SQLException}.
 *
 * <p>This interface is meant to be used wherever you need to operate on a {@code ResultSet}.</p>
 *
 * @see java.util.function.Consumer
 */
@FunctionalInterface
public interface ResultSetConsumer {

	/**
	 * Performs an operation on the given {@code ResultSet}.
	 *
	 * <p>This method is designed to perform an action using the provided {@code ResultSet},
	 * and may throw an {@code SQLException} if an error occurs during the operation.</p>
	 *
	 * @param rs the {@code ResultSet} on which to perform the operation
	 * @throws SQLException if an SQL error occurs during the operation
	 */
	void accept(ResultSet rs) throws SQLException;
}