package dev.tacon.database.function;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * A functional interface for running a series of operations within a single SQL transaction.
 * This interface is intended to be used for transactional operations that don't return a result.
 */
@FunctionalInterface
public interface TransactionalOperation {

	/**
	 * Executes a series of operations within a transaction on the given {@code Connection}.
	 *
	 * <p>This method should contain all the operations that are to be executed within a single transaction.
	 * If an SQLException occurs, it's expected that the transaction will be rolled back by the caller.</p>
	 *
	 * @param connection the {@code Connection} on which the transaction is to be run
	 * @throws SQLException if any SQL error occurs during the transaction
	 */
	void runInTransaction(Connection connection) throws SQLException;
}
