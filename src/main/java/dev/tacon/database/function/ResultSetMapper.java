package dev.tacon.database.function;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A functional interface representing a mapping operation that accepts a {@link ResultSet} argument
 * and returns a result of type {@code R} based on the current row. The operation may throw an {@link SQLException}.
 *
 * @param <R> the type of result produced by the mapper
 *
 * @see java.util.function.Function
 */
@FunctionalInterface
public interface ResultSetMapper<R> {

	/**
	 * Applies a mapping operation on the given {@code ResultSet} based on the current row and returns a result.
	 *
	 * <p>This method is designed to transform the provided {@code ResultSet} row into a result of type {@code R},
	 * and may throw an {@code SQLException} if an error occurs during the transformation.</p>
	 *
	 * @param rs the {@code ResultSet} to be mapped
	 * @return the result of applying the mapping operation to the {@code ResultSet}
	 * @throws SQLException if an SQL error occurs during the mapping operation
	 */
	R apply(ResultSet rs) throws SQLException;
}